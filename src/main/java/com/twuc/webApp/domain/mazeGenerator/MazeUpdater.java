package com.twuc.webApp.domain.mazeGenerator;

/**
 * 这个接口用于更新迷宫的 {@link Grid}。这个更新可能是生成新的迷宫，可能是在 Grid 中的 tags 中插入新的信息。
 */
public interface MazeUpdater {

    /**
     * 更新迷宫的 {@link Grid}
     *
     * @param grid 迷宫网格。
     */
    void update(Grid grid);
}
