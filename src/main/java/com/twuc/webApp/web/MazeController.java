package com.twuc.webApp.web;

import com.twuc.webApp.service.GameLevelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;

@RestController
public class MazeController {
    private final GameLevelService gameLevelService;
    private static final Logger logger = LoggerFactory.getLogger(MazeController.class);

    public MazeController(GameLevelService gameLevelService) {
        this.gameLevelService = gameLevelService;
    }

    @GetMapping("/mazes/{type}")
    @ResponseStatus(HttpStatus.OK)
    public void getMaze2(
        HttpServletResponse response,
        @RequestParam(required = false, defaultValue = "10") int width,
        @RequestParam(required = false, defaultValue = "10") int height,
        @PathVariable String type) throws IOException {

        response.setContentType(MediaType.IMAGE_PNG_VALUE);

        gameLevelService.renderMaze(
            response.getOutputStream(),
            width,
            height,
            type
        );
    }

    @ExceptionHandler
    public ResponseEntity<String> handleIllegalArgumentException(IllegalArgumentException e) {
        logger.error(LocalDateTime.now() + " " + e.getMessage());
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .contentType(MediaType.APPLICATION_JSON)
                .body("{\"message\": \""+ e.getMessage() +"\"}");
    }
}
