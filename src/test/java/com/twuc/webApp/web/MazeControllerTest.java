package com.twuc.webApp.web;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
public class MazeControllerTest {
    @Autowired
    TestRestTemplate template;

    @Test
    void should_return_400_and_error_msg_when_type_not_correct() {
        ResponseEntity<String> entity = template.getForEntity("/mazes/abc", String.class);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, entity.getStatusCode());
        Assertions.assertEquals(MediaType.APPLICATION_JSON, entity.getHeaders().getContentType());
        Assertions.assertEquals("{\"message\": \"Invalid type: abc\"}", entity.getBody());
    }

    @Test
    void should_return_400_and_error_msg_when_height_not_correct() {
        ResponseEntity<String> entity = template.getForEntity("/mazes/grass?width=0&height=5", String.class);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, entity.getStatusCode());
        Assertions.assertEquals(MediaType.APPLICATION_JSON, entity.getHeaders().getContentType());
        Assertions.assertEquals("{\"message\": \"The width(0) is not valid\"}", entity.getBody());
    }

    @Test
    void should_return_400_and_error_msg_when_width_not_correct() {
        ResponseEntity<String> entity = template.getForEntity("/mazes/grass?width=5&height=0", String.class);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, entity.getStatusCode());
        Assertions.assertEquals(MediaType.APPLICATION_JSON, entity.getHeaders().getContentType());
        Assertions.assertEquals("{\"message\": \"The height(0) is not valid\"}", entity.getBody());
    }

}
